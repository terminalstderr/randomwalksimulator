//
// Created by Ryan on 04/08/2016.
//
#include <getopt.h>
#include <iostream>
#include <cstdlib>

#include "RandomWalkSimulator.h"
#include "types.h"

using namespace std;

void print_progress(std::string out) {
  std::cout << out << std::endl;
}

void usage(char *argv[]) {
  cout << "USAGE: " << argv[0] << " [v] <infile> <partfile> <rounds> <partitions>" << endl;
  exit(1);
}

int main(int argc, char *argv[]) {
  // Parse argv
  bool verbose_mode = false;
  int opt;

  opterr = 0;
  while ((opt = getopt (argc, argv, "v")) != -1)
  {
    switch (opt)
    {
      case 'v':
        verbose_mode = true;
        break;
      default:
        usage(argv);
    }
  }

  // ensure the correct number of mando arguments is provided
  if (argc - optind != 4)
    usage(argv);
  string infile = argv[optind];
  string partition_file = argv[optind+1];
  uint64_t iterations = strtoul(argv[optind+2], NULL, 10);
  //uint64_t partitions = strtoul(argv[optind+3], NULL, 10); // TODO: where does our partitions come from?

  // Setup
  RandomWalkSimulator rws(infile, partition_file, iterations);
  rws.init(argc, argv);
  rws.setVerbose(verbose_mode);
  rws.registerProgressCallback(print_progress);
  rws.run();
  rws.finalize();
  exit(0);
}

