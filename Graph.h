//
// Created by Ryan on 04/07/2016.
//
// Assumption: Node IDs will be contiguous
//  i.e. if we have a node "100" then we assume that we will have node 1-99 as well.
#ifndef PROJ1_GRAPH_H
#define PROJ1_GRAPH_H

#include <string>
#include <unordered_map>
#include <mpi.h>

#include "Node.h"
#include "types.h"

/**
 * Stores a basic credit request
 */
struct CreditRequest {
  uint64_t node_id;
  uint64_t time;
};

/**
 * Basic MPI Tags we will use
 */
enum MpiTags {
  REQUEST_TAG = 0,
  REPLY_TAG = 1,
  FINISH_ROUND_TAG = 2
};

/**
 * Stores some basic static node information that we will cache local to each graph for _all_ nodes. This is
 */
struct NodeMetaInfo {
  NodeMetaInfo(uint64_t degree, int rank) : degree(degree), rank(rank) {};
  uint64_t degree;
  int rank;
};

class Graph {
public:

  // Define an MPI datatype for the Credit Request
  MPI_Datatype MPI_CREDIT_REQUEST;
  static const uint64_t DEFAULT_GRAPH_SIZE = 65536;
  /// Performs load during construction
  Graph(const std::string edge_filename, const std::string partition_filename, const int rank, const int max_rank, const int iterations);
  /// wait for all graph partitions to finish execution
  void wait_for_round_finish();
  /// Compute a step for the entire graph
  void compute();
  /// Compute a step for the subgraph indicated by [startIndex, endIndex] (inclusive)
  void compute_sub(uint64_t startIndex, uint64_t endIndex);
  /// Compute a step for the node indicated by the index
  void compute_node(uint64_t index);
  void store(const std::string filename);
  void check_valid();
  std::vector<Node*> *nodesptr;
  std::vector<Node*> nodes;
  char *tmp;
  char *fmt;

private:
  bool first = true;
  // Store our event management state
  MPI_Request *incoming_mpireqs;
  CreditRequest *incoming_credit_requests;
  // Misc debugging variable
  int ret;
  /// The nodes of the graph -- using a map for quick lookup
  std::unordered_map<uint64_t, NodeMetaInfo> *nodeid_to_infoptr;
  std::unordered_map<uint64_t, NodeMetaInfo> nodeid_to_info;
  std::unordered_map<uint64_t, std::vector<double> > *creditsptr;
  std::unordered_map<uint64_t, std::vector<double> > credits;
  int m_rank;
  int max_rank; // can assume that rank 0 through max_rank are valid
  int m_iterations;

public:
  // Graph management and helpers
  Node *get_local_node(uint64_t node_id);
  bool is_node_local(uint64_t node_id);
  double get_node_credit(uint64_t node_id, uint64_t time);
  NodeMetaInfo *get_node_info(uint64_t node_id);

private:
  void init_incoming_requests();
  void handle_incoming_requests();
  // Load and load helpers
  void load(const std::string edge_filename, const std::string partition_filename);
  void load_nodes(const std::string partition_filename);
  void load_edges(const std::string filename);
  void load_file_to_buffer(const std::string filename, char **buffer);
  void add_node(uint64_t nodeid, uint64_t degree, int rank);
  void add_edge(uint64_t node1id, uint64_t node2id);
  char *prepare_pretty_print(std::vector<double> array);

};


#endif //PROJ1_GRAPH_H
