//
// Created by Ryan on 04/07/2016.
//
#include <cstring>
#include <ctime>
#include <sstream>

#include "RandomWalkSimulator.h"
#include "types.h"

RandomWalkSimulator::RandomWalkSimulator(std::string edge_file_path, std::string partition_file_path, uint64_t iterations)
    : edgeFilePath(edge_file_path)
    , partitionFilePath(partition_file_path)
    , iterations(iterations)
    , verbose(false) { }

int RandomWalkSimulator::init(int argc, char *argv[]) {
  // Setup MPI -- use this information 
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &max_rank);
  // Setup outfilepath
  outFilePath = "./Partition" + std::to_string(rank) + ".out";
  LOG("Random Walk Simulator setup partition: " << rank);
  return 0;
}

void RandomWalkSimulator::finalize() {
  MPI_Finalize();
}

void RandomWalkSimulator::_step_system()
{
  // Basic graph operations
  graph->compute();
  LOG("Going to wait for round to finish before taking next step");
  graph->wait_for_round_finish();
  LOG("Round finished!");
  // TODO: rewrite check valid to instead be a 'creditsum' method.
  // graph->check_valid();
}

void RandomWalkSimulator::run()
{
  time_t stime, dtime;
  std::ostringstream strbuilder;

  // TODO: Add rank to print statements!
  // Graph load with output
  progress_callback_V("=====\nStarting Graph Load...");
  time(&stime);
  graph = new Graph(edgeFilePath, partitionFilePath, rank, max_rank, iterations);
  dtime = time(&dtime) - stime;
  strbuilder << "time to read input file, partition " << rank << " = " << dtime << "sec";
  progress_callback(strbuilder.str());
  strbuilder.str("");
  strbuilder.clear();
  progress_callback_V("Finished Graph Load...\n=====\n");

  // Graph compute with output
  progress_callback_V("=====\nStarting Graph Computing...");
  for (uint64_t i = 0; i < iterations; i++) {
    time(&stime);
    _step_system();
    dtime = time(&dtime) - stime;
    strbuilder << "time for round " << i+1 << ", partition " << rank << "  = " << dtime << "sec";
    progress_callback(strbuilder.str());
    strbuilder.str("");
    strbuilder.clear();
  }
  progress_callback_V("Finished Graph Computing...\n=====\n");

  // Graph save with output
  progress_callback_V("=====\nStarting Graph Save...");
  time(&stime);
  graph->store(outFilePath);
  dtime = time(&dtime) - stime;
  strbuilder << "time to write output file, partition " << rank << "  = " << dtime << "sec";
  progress_callback(strbuilder.str());
  progress_callback_V("Finished Graph Save...\n=====\n");
}

void RandomWalkSimulator::registerProgressCallback(CallBack prog_callback)
{
  progress_callback = prog_callback;
}

void RandomWalkSimulator::progress_callback_V(std::string str)
{
  if (verbose){
    progress_callback(str);
  }
}

void RandomWalkSimulator::setVerbose(bool verbosep)
{
 verbose = verbosep;
}





