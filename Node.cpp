//
// Created by Ryan on 04/08/2016.
//
#include "Node.h"
#include "types.h"

Node::Node(uint64_t id, uint64_t precomputed_degree)
    : id(id)
    , degree(0)
    , my_time(0)
    , credit(1.0f)
    , last_credit(1.0f)
    , out_edges_nodeid(new std::vector<uint64_t>())
{
  out_edges_nodeid->reserve(precomputed_degree);
}

Node::Node(uint64_t id)
    : id(id)
    , degree(0)
    , my_time(0)
    , credit(1.0f)
    , last_credit(1.0f)
    , out_edges_nodeid(new std::vector<uint64_t>())
{
  out_edges_nodeid->reserve(DEFAULT_OUTEDGE_SIZE);
}

void Node::AddOutEdge(const uint64_t &node_id)
{
  // Add the outedge
  out_edges_nodeid->push_back(node_id);
  degree++;
}

//void Node::AddBiEdge(Node *other)
//{
//  AddOutEdge(other);
//  other->AddOutEdge(this);
//}
