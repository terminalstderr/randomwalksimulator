#Random Walk Simulator#
CIS630, Spring 2016, Term Project --
*June 02, 2016*

*NOTICE*: Please see the discussion at the bottom of this readme towards a more performant Random Walk Simulation.

## Building ##
How do I compile this program on ix?

Run 'make' with no arguments: i.e.
```
#!bash

$ make
```

  The executable produced will be named ``rwsim.'' An example usage on the ix server might be:
```
#!bash
$ mpiexec.mpich -np 4 rwsim /cs/classes/www/16S/cis630/FL/fl_compact.tab 
                    /cs/classes/www/16S/cis630/FL/fl_compact_part.4 5 4
```


## Notes ##

### Graph Partitioning ###
This random walk simulator can handle inputs with 2 and 4 partitions.

### Choke Point ###
The choke point for a very large graph would be keys to a meta-info data structure that **each node** is maintaining. This data structure is used to know the location of each node (e.g. mapping node_id to process_id). This is being indexed by 64 bit unsigned long type so we will not hit this limitation in practice.

### Key Performance Indicators ###
It takes 9~14 seconds for input and output on ix reading ~110,000,000 edges.
It takes ~40 seconds per round of computation on ix for input size ~110,000,000 edges.

The goals for my communication model were:

1. extend non-parallelized work, e.g. do not rebuild this product from scratch
2. ensure that addition of nodes scales

I have achieved both of my goals. For the first goal, I was able to use MPI in my existing implementation while maintaining my original computation model. Furthermore, when scaling my implementation to more nodes, the process footprint should scale down accordingly with the exception of one meta-info data structure which is an acceptable cost.

## Discussion ##

  It is now clear that performing this transparent implementation is insufficient towards our performance goals. A future implementation would require a restructure of both my computation and communication models. I would take advantage of asynchronous communication, and the fact that our graph is static with bidirectional edges.
  
  A simple 2-stage implementation that leverages the bidirectional property of our graph and that the graph is static would be to perform a communication stage at the start of the round then perform computation in a second stage. A psuedo-algorithm for computing 'n' rounds:

1. Setup nonblocking receive buffer that is sizeof(outedges) -- MPI_Irecv
2. Synchronize all processes -- MPI_Barrier
3. Send credits to all outedges of the form (nodeid, credit) -- MPI_Ssend
    * Notice, we know that there is a receiver waiting since edges are bidirectional
4. Wait for nonblocking receive to complete -- MPI_Wait for step #1
5. Perform compute step using combined knowledge of the received (nodeid, credit) values and the locally maintained graph partition.
6. Until performed 'n' rounds, goto step 1