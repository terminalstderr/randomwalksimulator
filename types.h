//
// Created by Ryan on 04/17/2016.
//
#ifndef PROJ1_TYPES_H
#define PROJ1_TYPES_H

#include <iostream>

//#define LOGGING
#ifdef LOGGING
#define LOG(msg) \
    std::cout << __FILE__ << "(" << __LINE__ << "): " << msg << std::endl 
#else
#define LOG(msg) 
#endif


typedef unsigned long uint64_t;

#endif //PROJ1_TYPES_H
