//
// Created by Ryan on 04/07/2016.
//
#ifndef PROJ1_RANDOMWALKSIMULATOR_H
#define PROJ1_RANDOMWALKSIMULATOR_H

#include "Graph.h"
#include "types.h"

class RandomWalkSimulator {
private:
  int rank;
  int max_rank;
  std::string edgeFilePath;
  std::string partitionFilePath;
  std::string outFilePath;
  Graph *graph;
  uint64_t iterations;
  bool verbose;
  void progress_callback_V(std::string str);
public:
  typedef void (*CallBack)(std::string);
  RandomWalkSimulator(std::string edge_file_path, std::string partition_file_path, uint64_t iterations);
  int init(int argc, char *argv[]);
  void finalize();
  void run();
  void registerProgressCallback(CallBack prog_callback);
  void _step_system();
  void setVerbose(bool verbose);

private:
  CallBack progress_callback;
};


#endif //PROJ1_RANDOMWALKSIMULATOR_H
