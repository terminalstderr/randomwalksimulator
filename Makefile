PARTITIONS = 2
HEADERS = Graph.h Node.h RandomWalkSimulator.h
OBJECTS = Graph.o Node.o RandomWalkSimulator.o main.o
#CPPFLAGS = -DLOGGING -Wall -Werror -g -std=gnu++11
#CPPFLAGS = -Wall -Werror -g -std=gnu++11
CPPFLAGS = -Wall -Werror -O4 -march=native -std=gnu++11
MPIFLAGS = -np $(PARTITIONS) -fast

CPP=mpicxx.mpich2
RUN=mpiexec.mpich

default: rwsim

%.o: %.cpp $(HEADERS)
	$(CPP) $(CPPFLAGS) -c $< -o $@

rwsim: $(OBJECTS)
	$(CPP) $(CFLAGS) $(OBJECTS) -o $@

large_test: rwsim
	$(RUN) -np 4 rwsim inputs/fl_compact.tab inputs/fl_compact_part.4 5 4

small_test: rwsim
	$(RUN) $(MPIFLAGS) rwsim  inputs/test.tab inputs/test_part.2	5	2

trivial_test: rwsim
	$(RUN) $(MPIFLAGS) rwsim inputs/test_perf.tab inputs/test_perf_part.2	10 2

assym_test: rwsim
	$(RUN) $(MPIFLAGS) rwsim inputs/test_perf_assym.tab inputs/test_perf_assym_part.2	10 2

mpi2_test: rwsim
	$(RUN) $(MPIFLAGS) rwsim inputs/test_mpi.tab inputs/test_mpi_part.2	2 2


clean:
	-rm -f $(OBJECTS)
	-rm -f rwsim
