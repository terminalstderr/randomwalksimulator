//
// Created by Ryan on 04/08/2016.
//
#ifndef PROJ1_NODE_H
#define PROJ1_NODE_H

#include <vector>

#include "types.h"


class Node {
public:
  /// Default number of output edges to allocate
  static const uint64_t DEFAULT_OUTEDGE_SIZE = 4;
  /// Node ID
  uint64_t id;
  /// Node's degree
  uint64_t degree;
  /// Time of this node
  uint64_t my_time;
  /// Credit of this node at time my_time
  double credit;
  /// Credit of this node at time my_time - 1
  double last_credit;
  /// Node's outedges
  std::vector<uint64_t> *out_edges_nodeid;
public:
  // Constructor
  Node(uint64_t id);
  Node(uint64_t id, uint64_t precomputed_degree);
  // Modifiers
  void AddOutEdge(const uint64_t &node_id);
  // void AddBiEdge(Node *other);
};

#endif //PROJ1_NODE_H
