//
// Created by Ryan on 04/07/2016.
//
#include <algorithm>
#include <cstddef>
#include <cstdarg>
#include <cstring>
#include <cstdio>
#include <unordered_map>
#include <sstream>
#include <stdexcept>
#include <mpi.h>
#include <iomanip>

#include "Graph.h"
#include "types.h"

Graph::Graph(const std::string edge_filename, const std::string partition_filename, const int rank, const int max_rank, const int iterations)
    : ret(0), m_rank(rank), max_rank(max_rank), m_iterations(iterations)
{
  
  // Setup our MPI data structure for credit requests
  const int nitems=2;
  int blocklengths[2] = {1,1};
  MPI_Datatype types[2] = {MPI_UNSIGNED_LONG_LONG, MPI_UNSIGNED_LONG_LONG};
  MPI_Aint offsets[2];
  offsets[0] = offsetof(CreditRequest, node_id);
  offsets[1] = offsetof(CreditRequest, time);
  MPI_Type_create_struct(nitems, blocklengths, offsets, types, &MPI_CREDIT_REQUEST);
  MPI_Type_commit(&MPI_CREDIT_REQUEST);
  // Setup our receivers
  incoming_mpireqs = new MPI_Request[max_rank];
  incoming_credit_requests = new CreditRequest[max_rank];
  // Setup our datastructures
  nodesptr = new std::vector<Node *>();
  nodes = *nodesptr;
  nodes.reserve(DEFAULT_GRAPH_SIZE);
  nodeid_to_infoptr = new std::unordered_map<uint64_t, NodeMetaInfo>();
  nodeid_to_info = *nodeid_to_infoptr;
  nodeid_to_info.reserve(DEFAULT_GRAPH_SIZE);
  // Perform the load
  load(edge_filename, partition_filename);
  creditsptr = new std::unordered_map<uint64_t, std::vector<double> >();
  credits = *creditsptr;
  credits.reserve(DEFAULT_GRAPH_SIZE);
}

void mpi_log(int ret) {
  if (ret != 0)
    LOG("================================================================================\n\nMPI ERROR: " << ret);
}

void Graph::handle_incoming_requests() {
      int incoming_done;
      MPI_Status incoming_status;
      for (int i = 0; i < max_rank ; i++) {
        // Don't wait for this rank
        if (i == m_rank)
          continue;
        
        ret = MPI_Test(&incoming_mpireqs[i], &incoming_done, &incoming_status); mpi_log(ret);
        if (incoming_done) {
          // We received a request! Have to handlek it!
          // Send a response
          double reply;
          Node *node = get_local_node(incoming_credit_requests[i].node_id);
          if (incoming_credit_requests[i].time == node->my_time)
            reply = node->credit;
          else if (incoming_credit_requests[i].time + 1 == node->my_time)
            reply = node->last_credit;
          ret = MPI_Ssend(&reply, 1, MPI_DOUBLE,  
                          incoming_status.MPI_SOURCE, REPLY_TAG, MPI_COMM_WORLD);
          mpi_log(ret);

          // Prepare to handle another request
          ret = MPI_Irecv(&incoming_credit_requests[i], 1, MPI_CREDIT_REQUEST,  
                    i, REQUEST_TAG, MPI_COMM_WORLD,
                    &incoming_mpireqs[i]);
          mpi_log(ret);
        }
    }
}

void Graph::init_incoming_requests() {
  for (int i = 0; i < max_rank ; i++) {
    // Don't recv for this rank
    if (i == m_rank)
      continue;
    ret = MPI_Irecv(&incoming_credit_requests[i], 1, MPI_CREDIT_REQUEST, 
              i, REQUEST_TAG, MPI_COMM_WORLD,
              &incoming_mpireqs[i]);
    mpi_log(ret);
  }
}


void Graph::wait_for_round_finish() {
    // If we are in wait, we assume that a request is already listening
    /*
    // Recieve incoming requests
    MPI_Request incoming_mpireq;
    CreditRequest incoming_credit_request;
    ret = MPI_Irecv(&incoming_credit_request, 1, MPI_CREDIT_REQUEST,  
              MPI_ANY_SOURCE, REQUEST_TAG, MPI_COMM_WORLD,
              &incoming_mpireq);
    mpi_log(ret);
    */

    // TODO: Somehow calling Ibarrier and IRecv is causing a deadlock... hmm
    LOG("MPI: Setting up barrier" << m_rank);
    MPI_Request barrier_mpireq;
    ret = MPI_Ibarrier(MPI_COMM_WORLD, &barrier_mpireq); mpi_log(ret);

    int barrier_done = 0;
    MPI_Status barrier_status;
    LOG("MPI: Going to wait for barrier to complete and incoming requests" << m_rank);
    while(true) {
      handle_incoming_requests();
      ret = MPI_Test(&barrier_mpireq, &barrier_done, &barrier_status); mpi_log(ret);
      if (barrier_done) {
        LOG("MPI: === Barrier finished! === " << m_rank);
        //MPI_Cancel(&incoming_mpireq);
        return;
      }
    }
}

void Graph::compute() {
  for (Node* node : nodes) {
    compute_node(node->id);
  }
}

bool node_node_compare_lt(const Node *node1, const Node *node2) {
  return node1->id < node2->id;
}

bool node_nodeid_compare_lt(const Node *node, const uint64_t node_id) {
  return node->id < node_id;
}


// O(log(n/1))
NodeMetaInfo *Graph::get_node_info(uint64_t node_id) {
  std::unordered_map<unsigned long, NodeMetaInfo>::iterator found = nodeid_to_info.find(node_id);
  if (found->first != node_id) {
    throw std::runtime_error("Could not find node!");
  }
  return &(found->second);
}

// O(log(n/1))
bool Graph::is_node_local(uint64_t node_id) {
  NodeMetaInfo *info = get_node_info(node_id);
  return info->rank == m_rank;
}

// O(log(n/c))
Node *Graph::get_local_node(uint64_t node_id) {
  if (!is_node_local(node_id)) {
    throw std::runtime_error("Node is not local!");
  }

  // Find our object locally O(logn)
  std::vector<Node *>::iterator found = std::lower_bound(nodes.begin(), nodes.end(), node_id, node_nodeid_compare_lt);
  Node *node = *found;

  // Make sure that our search was successful
  if (node->id != node_id)
    throw std::runtime_error("Search for local node failed!");

  return node;
}

// O(log(n))
double Graph::get_node_credit(uint64_t node_id, uint64_t time) {
  if (is_node_local(node_id))
  { // Get credit from local graph
    // Find our object locally O(logn)
    Node *node = get_local_node(node_id);

    // find credit based on time
    if (time == node->my_time)
      return node->credit;
    else if (time + 1 == node->my_time)
      return node->last_credit;
    else
      throw std::runtime_error("Timing discrepency!");
  }
  else
  {
    // Recieve incoming requests
    //MPI_Request incoming_mpireq;
    //CreditRequest incoming_credit_request;
    // Want to only do this FIRST TIME
    if (first)  {
      first = false;
      init_incoming_requests();
    }

    // Send outbound request
    NodeMetaInfo *info = get_node_info(node_id);
    CreditRequest my_credit_request;
    my_credit_request.node_id = node_id;
    my_credit_request.time = time;
    // info is good
    LOG("PID: " << m_rank << " Blocking Send! XXX");
    ret = MPI_Ssend(&my_credit_request, 1, MPI_CREDIT_REQUEST,
              info->rank, REQUEST_TAG, MPI_COMM_WORLD);
    mpi_log(ret);
    LOG("PID: " << m_rank << " FINI         ! XXX");

    // Await my incoming reply -- only recv from the rank that we made request to
    MPI_Request my_mpireq;
    double my_credit_reply;
    ret = MPI_Irecv(&my_credit_reply, 1, MPI_DOUBLE, 
              info->rank, REPLY_TAG, MPI_COMM_WORLD,
              &my_mpireq);
    mpi_log(ret);

    // While my request has not completed, complete any incoming request
    int my_done = 0;  
    MPI_Status my_status;
    while (true) {
      handle_incoming_requests();

      ret = MPI_Test(&my_mpireq, &my_done, &my_status); mpi_log(ret);

      if (my_done) {
        // TODO -- what happens if we cancel when the request has been successfully completed? Essentially we loose one message!
        double tmp = my_credit_reply;
        //MPI_Cancel(&incoming_mpireq);
        return tmp;
      }
    }
  }
}

void Graph::compute_node(uint64_t node_id) {
  Node *me = get_local_node(node_id);
  std::vector<uint64_t>::iterator neighbor = me->out_edges_nodeid->begin();
  double sum = 0.0;
  for ( ; neighbor != me->out_edges_nodeid->end() ; neighbor++ )
  {
    double credit = get_node_credit(*neighbor, me->my_time);
    sum += credit / get_node_info(*neighbor)->degree;
  }
  // CRITICAL SECTION
  {
    // Update node Data Structure
    me->last_credit = me->credit;
    me->credit = sum;
    me->my_time++;
    // Update credits Data Structure
    credits[me->id].push_back(me->credit);
  }
}

//
void Graph::load(const std::string edge_filename, const std::string partition_filename) {
  load_nodes(partition_filename);
  // Have to sort the nodes once loaded!
  std::sort(nodes.begin(), nodes.end(), node_node_compare_lt);
  load_edges(edge_filename);
}

// PERFORMS ALLOCATION: Make sure to free(buffer) when done with it!
void Graph::load_file_to_buffer(const std::string filename, char **buffer_ptr)
{
  // Open file for reading as bytestream
  FILE *inFile = fopen(filename.c_str(), "r");
  if (inFile == NULL)
  {
    throw std::runtime_error("Unable to open file (fopen failed)!");
  }

  // obtain file size
  fseek(inFile, 0, SEEK_END);
  long fileSizeL = ftell(inFile);
  if (fileSizeL == -1L)
  {
    throw std::runtime_error("Unable to open file (get file size failed)!");
  }
  size_t fileSize = fileSizeL;
  rewind(inFile);

  // allocate memory to contain the whole file:
  *buffer_ptr = (char *) malloc(sizeof(char) * fileSize);
  if (*buffer_ptr == NULL)
  {
    throw std::runtime_error("Unable to open file (malloc failed)!");
  }

  // copy the file into the buffer:
  size_t result = fread(*buffer_ptr, 1, fileSize, inFile);
  if (result != fileSize)
  {
    throw std::runtime_error("Unable to open file (size didn't match up)!");
  }
  fclose (inFile);
}

void Graph::load_nodes(const std::string partition_filename) {
  char *buffer;
  load_file_to_buffer(partition_filename, &buffer);

  // Perform tokenizing of the data read into memory
  // We boldly assume that our file will be formatted correctly -- if there are any formatting errors then we will get
  // silent failures at this point...
  // From the project specifications:
  //    "You can assume that the input file has a format exactly similar to the sample input file."
  uint64_t node_id;
  uint64_t degree;
  int rank;
  char *parse_buf = buffer;
  char *parse_str;
  char *saveptr;
  while(true) {

    // Get ID
    parse_str = strtok_r(parse_buf, "\t\n", &saveptr);
    if (!parse_str) {
      break;
    } // good behavior
    node_id = strtoul(parse_str, NULL, 10);

    // Managing strtok call correctly
    parse_buf = NULL;

    // Get degree
    parse_str = strtok_r(parse_buf, "\t\n", &saveptr);
    if (!parse_str) {
      break;
    }
    degree = strtoul(parse_str, NULL, 10);

    // Get rank
    parse_str = strtok_r(parse_buf, "\t\n", &saveptr);
    if (!parse_str) {
      break;
    }
    rank = std::stoi(parse_str, NULL, 10);

    // Build our node!
    add_node(node_id, degree, rank);
  }

  // Terminate
  free (buffer);
}

void Graph::load_edges(const std::string edge_filename)
{
  char *buffer;
  load_file_to_buffer(edge_filename, &buffer);

  // Perform tokenizing of the data read into memory
  // We boldly assume that our file will be formatted correctly -- if there are any formatting errors then we will get
  // silent failures at this point...
  // From the project specifications:
  //    "You can assume that the input file has a format exactly similar to the sample input file."
  uint64_t id1;
  uint64_t id2;
  char *parse_buf = buffer;
  char *idstr, *saveptr;
  while(true) {

    // Get ID1
    idstr = strtok_r(parse_buf, "\t\n", &saveptr);
    if (!idstr) {
      break;
    } // good behavior
    id1 = strtoul(idstr, NULL, 10);

    // Managing strtok call correctly
    parse_buf = NULL;

    // Get ID2
    idstr = strtok_r(parse_buf, "\t\n", &saveptr);
    if (!idstr) {
      break;
    }
    id2 = strtoul(idstr, NULL, 10);

    // Build our graph!
    add_edge(id1, id2);
  }

  // terminate
  free (buffer);
}

void Graph::add_node(uint64_t nodeid, uint64_t degree, int rank)
{
  const NodeMetaInfo &info = NodeMetaInfo(degree, rank);
  nodeid_to_info.insert(std::pair<uint64_t, NodeMetaInfo>(nodeid, info));

  if (rank == m_rank)
  {
    nodes.push_back(new Node(nodeid, degree));
    credits[nodeid] = std::vector<double>();
    credits[nodeid].reserve(m_iterations);
  
  }
}

// Assumes that the nodes already exist
void Graph::add_edge(uint64_t node1id, uint64_t node2id)
{
  // Do nothing if the nodes have same ID
  if (node1id == node2id)
    // Log message indicating the fault
    return;
  Node *node = NULL;
  if (is_node_local(node1id)) {
    node = get_local_node(node1id);
    node->AddOutEdge(node2id);
  }
  if (is_node_local(node2id)) {
    node = get_local_node(node2id);
    node->AddOutEdge(node1id);
  }
}

void Graph::check_valid()
{
  bool first = true;
  uint64_t time = 0;
  double credit_sum = 0;
  double credit_sum_expected = 0;
  for (uint64_t i = 0; i < nodes.size(); i++) // is this checking every node?
  {
    if (nodes[i] == NULL)
      continue;
    if (first)
    {
      time = nodes[i]->my_time;
      first = false;
    }

    if (time != nodes[i]->my_time)
    {
      throw std::runtime_error("Timing discrepancy during check!");
    }
    credit_sum += nodes[i]->credit;
    credit_sum_expected += double(1.0);
  }

  if (credit_sum - credit_sum_expected > 0.000005 ||
      credit_sum - credit_sum_expected < -0.000005)
  {
    throw std::runtime_error("Credit didn't sum correctly!");
  }
}

void Graph::store(const std::string filename)
{
  // Open file for writing as bytestream
  FILE *outFile = fopen (filename.c_str(), "wb");
  if (outFile==NULL)
  {
    throw std::runtime_error("Unable to open store file (fopen failed)!");
  }

  // Perform the writing
  for (Node* node : nodes) {
    fprintf(outFile, "%lu\t%lu\t", node->id, node->degree);
    for (double credit : credits[node->id])
      fprintf(outFile, "%0.6f\t", credit);
    fseek(outFile,-1, SEEK_CUR);
    fprintf(outFile, "\n");
  }


  // Cleanup resources
  fclose(outFile);
}

char *Graph::prepare_pretty_print(std::vector<double> array)
{
  //snprintf(tmp, sizeof(tmp)-1, "%f", );
  std::ostringstream res;
  //res.precision(6);
  res << std::fixed << std::setprecision(6);
  for (size_t i = 0; i < array.size(); i++) {
    res << array[i] << "\t";
  }
  //char *ret = (char *) res.str().c_str();
  tmp = (char *) res.str().c_str();
  tmp[strlen(tmp)-1] = '\n';
  return tmp;
}

