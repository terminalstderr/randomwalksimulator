//
// Created by Ryan on 04/16/2016.
//

#ifndef PROJ1_CONSTANTS_H
#define PROJ1_CONSTANTS_H

const std::string test_clean_partition = "C:/Users/Ryan/OneDrive/Documents/distributedSystems/proj1/test/test_clean.part2";
const std::string test_clean_input = "C:/Users/Ryan/OneDrive/Documents/distributedSystems/proj1/test/test_clean.input";
const std::string flcompact_input = "C:/Users/Ryan/OneDrive/Documents/distributedSystems/proj1/test/fl_compact.tab";
const std::string flcompact2_partition = "C:/Users/Ryan/OneDrive/Documents/distributedSystems/proj1/test/fl_compact_part.2";
const std::string flcompact4_partition = "C:/Users/Ryan/OneDrive/Documents/distributedSystems/proj1/test/fl_compact_part.4";
const std::string test_partition = "C:/Users/Ryan/OneDrive/Documents/distributedSystems/proj1/test/test.part2";
const std::string test_input = "C:/Users/Ryan/OneDrive/Documents/distributedSystems/proj1/test/test.input";
const std::string flikr_input = "C:/Users/Ryan/Downloads/_cis630/fl_undir.tab.txt";
const std::string or_input = "C:/Users/Ryan/Downloads/_cis630/or_undir.tab.txt";
const std::string test_output_path = "C:/Users/Ryan/OneDrive/Documents/distributedSystems/proj1/test/test.out";


#endif //PROJ1_CONSTANTS_H
