//
// Created by Ryan on 04/09/2016.
//

#include "gtest/gtest.h"
#include "../RandomWalkSimulator.h"
#include "constants.h"

TEST(construct, basic_construction) {
  new RandomWalkSimulator(test_input, test_output_path + ".rws01", 5);
}

void print_progress(std::string out) {
  std::cout << out << std::endl;
}

TEST(run_system, take_10_steps) {
  RandomWalkSimulator *rws = new RandomWalkSimulator(test_input, test_output_path + ".rws02", 5);
  rws->registerProgressCallback(print_progress);
  rws->run();
}

TEST(run_system, flikr_1_step) {
  RandomWalkSimulator *rws = new RandomWalkSimulator(flikr_input, test_output_path + ".rws_1_flikr", 1);
  rws->registerProgressCallback(print_progress);
  rws->run();
}
