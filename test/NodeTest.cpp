//
// Created by Ryan on 04/08/2016.
//
#include "gtest/gtest.h"
#include "../Node.h"

TEST(node, construction)
{
  //ARRANGE
  Node *n = new Node(123456);
  //ASSERT
  ASSERT_EQ(n->out_edges->size(), 0);
  //ASSERT_EQ(n->out_edges->capacity(), Node::DEFAULT_OUTEDGE_SIZE);
  ASSERT_EQ(n->degree, 0);
  ASSERT_EQ(n->id, 123456);
  ASSERT_EQ(n->my_time, 0);
  ASSERT_EQ(n->credit, 1.0f);
  ASSERT_EQ(n->last_credit, 1.0f);
}

TEST(add_edge, five_elements_node_resize)
{
  //ARRANGE
  Node *n = new Node(100);
  //ACT
  for (uint64_t i = 5; i > 0; i--)
    n->AddOutEdge(new Node(i));
  //ASSERT
  ASSERT_EQ(n->degree, 5);
  ASSERT_EQ(n->out_edges->size(), 5);
}

TEST(add_edge, 100_elements_node_resize)
{
  //ARRANGE
  Node *n = new Node(100);
  //ACT
  for (uint64_t i = 100; i > 0; i--)
    n->AddOutEdge(new Node(i));
  //ASSERT
  ASSERT_EQ(n->degree, 100);
  ASSERT_EQ(n->out_edges->size(), 100);
}

TEST(add_bi_edge, basic)
{
  Node *n1 = new Node(1);
  Node *n2 = new Node(2);
  n1->AddBiEdge(n2);
  ASSERT_EQ(n1->degree, 1);
  ASSERT_EQ(n2->degree, 1);
}

