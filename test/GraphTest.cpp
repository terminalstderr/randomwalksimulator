//
// Created by Ryan on 04/09/2016.
//

#include <map>
#include "gtest/gtest.h"
#include "../Node.h"
#include "../Graph.h"
#include "constants.h"

////////////////////////////////////////////////////////////////////////////////////////////////////
/// Performs load during construction
////////////////////////////////////////////////////////////////////////////////////////////////////
TEST(construct, basic_construction) {
  Graph *g1 = new Graph(test_input, test_partition, 0);
  Graph *g2 = new Graph(test_input, test_partition, 1);

  ASSERT_EQ(g1->nodes.size(), 2);
  ASSERT_EQ(g2->nodes.size(), 2);
}

TEST(construct, basic_construction_of_clean) {
  Graph *g1 = new Graph(test_clean_input, test_clean_partition, 0);
  Graph *g2 = new Graph(test_clean_input, test_clean_partition, 1);

  ASSERT_EQ(g1->nodes.size(), 3);
  ASSERT_EQ(g2->nodes.size(), 2);
}

TEST(construct, DISABLED_big_construction) {
  Graph *g1 = new Graph(flcompact_input, flcompact2_partition, 0);
  Graph *g2 = new Graph(flcompact_input, flcompact2_partition, 1);

  ASSERT_TRUE(abs(g1->nodes.size() - g2->nodes.size()) < 20);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
//// Compute a step for the node indicated by the index
//////////////////////////////////////////////////////////////////////////////////////////////////////
TEST(compute_node_index, basic_compute_one_neighbor_deg3) {
  //ARRANGE
  Graph *g1 = new Graph(test_input, test_partition, 0);
  Graph *g2 = new Graph(test_input, test_partition, 1);
  //ACT
  g1->compute_node(1);
  //ASSERT
  ASSERT_DOUBLE_EQ(g1->nodes[1]->credit, 1.0/3.0);
  //ASSERT_DOUBLE_EQ(g->nodes[1]->last_credit, 1.0);
}

TEST(compute_node, basic_compute_internal_nodes) {
  Graph *g1 = new Graph(test_clean_input, test_clean_partition, 0);
  Graph *g2 = new Graph(test_clean_input, test_clean_partition, 1);

  g1->compute_node(2);
  g1->compute_node(1);
  g2->compute_node(5);

  ASSERT_DOUBLE_EQ(g1->get_local_node(1)->credit, 1.0/2.0 + 1.0/1.0);
  ASSERT_DOUBLE_EQ(g1->get_local_node(2)->credit, 1.0/2.0);
  ASSERT_DOUBLE_EQ(g2->get_local_node(5)->credit, 1.0/2.0 + 1.0/1.0);
}
//
//TEST(compute_node_index, basic_compute_two_neighbor_deg3_deg2) {
//  //ARRANGE
//  Graph *g = new Graph(test_input);
//  //ACT
//  g->compute_node(2);
//  //ASSERT
//  ASSERT_DOUBLE_EQ(g->nodes[2]->credit, 1.0/3.0 + 1.0/2.0);
//  ASSERT_DOUBLE_EQ(g->nodes[2]->last_credit, 1.0);
//}
//
//TEST(compute_node_index, basic_compute_three_neighbor_deg1_deg2_deg2) {
//  //ARRANGE
//  Graph *g = new Graph(test_input);
//  //ACT
//  g->compute_node(3);
//  //ASSERT
//  ASSERT_DOUBLE_EQ(g->nodes[3]->credit, 1.0/1.0 + 1.0/2.0 + 1.0/2.0);
//  ASSERT_DOUBLE_EQ(g->nodes[3]->last_credit, 1.0);
//}
//
//TEST(compute_node_index, basic_compute_two_neighbor_deg3_deg2_) {
//  //ARRANGE
//  Graph *g = new Graph(test_input);
//  //ACT
//  g->compute_node(4);
//  //ASSERT
//  ASSERT_DOUBLE_EQ(g->nodes[4]->credit, 1.0/2.0 + 1.0/3.0);
//  ASSERT_DOUBLE_EQ(g->nodes[4]->last_credit, 1.0);
//}
//
//TEST(compute_node_index_over_time, basic_compute_two_neighbor_deg3_deg2_) {
//  //ARRANGE
//  Graph *g = new Graph(test_input);
//  //ACT & Assert
//  g->compute_node(4);
//  ASSERT_THROW(g->compute_node(4), std::runtime_error);
//}
//
//////////////////////////////////////////////////////////////////////////////////////////////////////
///// Compute a step for the subgraph indicated by [startIndex, endIndex] (inclusive)
//////////////////////////////////////////////////////////////////////////////////////////////////////
//TEST(compute_subgraph, basic_compute) {
//  //ARRANGE
//  Graph *g = new Graph(test_input);
//  //ACT
//  g->compute_sub(3,4);
//  //ASSERT
//  ASSERT_DOUBLE_EQ(g->nodes[3]->credit, 1.0/1.0 + 1.0/2.0 + 1.0/2.0);
//  ASSERT_DOUBLE_EQ(g->nodes[3]->last_credit, 1.0);
//  ASSERT_DOUBLE_EQ(g->nodes[4]->credit, 1.0/2.0 + 1.0/3.0);
//  ASSERT_DOUBLE_EQ(g->nodes[4]->last_credit, 1.0);
//}
//
//TEST(compute_subgraph, too_large_index) {
//  //ARRANGE
//  Graph *g = new Graph(test_input);
//  //ACT & ASSERT
//  ASSERT_THROW(g->compute_sub(3,Graph::DEFAULT_GRAPH_SIZE*2), std::out_of_range);
//}
//
//////////////////////////////////////////////////////////////////////////////////////////////////////
///// Compute two steps for the entire graph
//////////////////////////////////////////////////////////////////////////////////////////////////////
//TEST(compute, basic_compute) {
//  //ARRANGE
//  Graph *g = new Graph(test_input);
//  //ACT
//  g->compute();
//  //ASSERT
//  ASSERT_DOUBLE_EQ(g->nodes[1]->credit, 1.0/3.0);
//  ASSERT_DOUBLE_EQ(g->nodes[1]->last_credit, 1.0);
//  ASSERT_DOUBLE_EQ(g->nodes[2]->credit, 1.0/3.0 + 1.0/2.0);
//  ASSERT_DOUBLE_EQ(g->nodes[2]->last_credit, 1.0);
//  ASSERT_DOUBLE_EQ(g->nodes[3]->credit, 1.0/1.0 + 1.0/2.0 + 1.0/2.0);
//  ASSERT_DOUBLE_EQ(g->nodes[3]->last_credit, 1.0);
//  ASSERT_DOUBLE_EQ(g->nodes[4]->credit, 1.0/2.0 + 1.0/3.0);
//  ASSERT_DOUBLE_EQ(g->nodes[4]->last_credit, 1.0);
//}
//
//TEST(compute, basic_compute_two_iterations) {
//  //ARRANGE
//  Graph *g = new Graph(test_input);
//  //ACT
//  g->compute();
//  g->compute();
//  //ASSERT
//  ASSERT_DOUBLE_EQ(g->nodes[1]->last_credit, 1.0/3.0);
//  ASSERT_DOUBLE_EQ(g->nodes[1]->credit, 2.0/3.0);
//  ASSERT_DOUBLE_EQ(g->nodes[2]->last_credit, 1.0/3.0 + 1.0/2.0);
//  ASSERT_DOUBLE_EQ(g->nodes[2]->credit, 13.0/12.0);
//  ASSERT_DOUBLE_EQ(g->nodes[3]->last_credit, 1.0/1.0 + 1.0/2.0 + 1.0/2.0);
//  ASSERT_DOUBLE_EQ(g->nodes[3]->credit, 7.0/6.0);
//  ASSERT_DOUBLE_EQ(g->nodes[4]->last_credit, 1.0/2.0 + 1.0/3.0);
//  ASSERT_DOUBLE_EQ(g->nodes[4]->credit, 13.0/12.0);
//  ASSERT_NO_THROW(g->check_valid());
//}
//
//////////////////////////////////////////////////////////////////////////////////////////////////////
///// Save graph results out to disk
//////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//TEST(store, store_basic_one_iteration_result) {
//  //ARRANGE
//  Graph *g = new Graph(test_input);
//  //ACT
//  g->compute();
//  //ASSERT
//  ASSERT_NO_THROW(g->store(test_output_path + ".01"));
//}
//
//TEST(store, store_basic_three_iteration_result) {
//  //ARRANGE
//  Graph *g = new Graph(test_input);
//  //ACT
//  g->compute();
//  g->compute();
//  g->compute();
//  //ASSERT
//  ASSERT_NO_THROW(g->store(test_output_path + ".02"));
//}
//
//TEST(store, store_basic_1000_iteration_result) {
//  //ARRANGE
//  Graph *g = new Graph(test_input);
//  //ACT
//  for (int i = 0; i < 1000; i++)
//    g->compute();
//  //ASSERT
//  ASSERT_NO_THROW(g->store(test_output_path + ".03"));
//}
//
//TEST(store, store_flikr_1_iteration_result) {
//  //ARRANGE
//  Graph *g = new Graph(flikr_input);
//  //ACT
//  g->compute();
//  //ASSERT
//  ASSERT_NO_THROW(g->store(test_output_path + ".04"));
//}
//
//TEST(store, store_flikr_5_iteration_result) {
//  //ARRANGE
//  Graph *g = new Graph(flikr_input);
//  //ACT
//  for (int i = 0; i < 5 ; i++)
//    g->compute();
//  //ASSERT
//  ASSERT_NO_THROW(g->store(test_output_path + ".05"));
//}
//
//TEST(store, store_or_1_iteration_result) {
//  //ARRANGE
//  Graph *g = new Graph(or_input);
//  //ACT
//  g->compute();
//  //ASSERT
//  ASSERT_NO_THROW(g->store(test_output_path + ".06"));
//}
//
//TEST(store, store_or_5_iteration_result) {
//  //ARRANGE
//  Graph *g = new Graph(or_input);
//  //ACT
//  for (int i = 0; i < 5 ; i++)
//    g->compute();
//  //ASSERT
//  ASSERT_NO_THROW(g->store(test_output_path + ".07"));
//}
